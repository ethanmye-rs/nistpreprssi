# Readme

Included in this repo is the main python analysis code (main.py), a rssi plotter (plotter.py), and csv data.

The old datasets indoorLOS and outdoorLOS use a 1m threshold for danger; outdoorinterferance and garage use a 1.5m threshold for danger.
The new datasets garage and test use a 1.5m threshold and sample channels 0-36

Generally, the files used for the analysis are named "before" and "after". all "before" data is before the 1.5M cutoff, and all data labeled "after" is after the 1.5m cutoff

The data is collected by starting and stopping both devices at a given distance (usually 50, 100, 150 etc) and recording the results. the data is then turned into a "stitched" csv, where the first ~1000 events are pulled at every before distance into a single file, ordered by distance, likewise for every after distance.

This allows me to label all the events before 1.5m as either true positives or false negatives, and all data after 1.5m as false positives or true negatives by the danger threshold set above.