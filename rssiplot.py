from matplotlib import pyplot as plt
from main import *

if __name__ == "__main__":
    analysisdir = r'csv\new\garage'
    channelchoice = 19
    mnscale = 10 # this is arbitrary; pick mnratio based on max number of samples, pick m to be 5% of dataset size.

    afterchannel = {n: [] for n in range(0,37)}
    beforechannel = {n: [] for n in range(0,37)}

    auclist = []
    delacech(analysisdir, 'before', beforechannel, afterchannel)
    delacech(analysisdir, 'after', beforechannel, afterchannel)
    mnratio = list(np.linspace(0.01,1,50))
    m = mnscale
    n = [floor(mnscale * mnratio[i]) for i in range(0, len(mnratio))]
    dblow = -80 #db search range to check
    dbhigh = -30

    beforech = [int(i) for i in beforechannel[channelchoice]]
    afterch =  [int(i) for i in afterchannel[channelchoice]]

    plt.plot(beforech)
    plt.title("distances <150cm")
    plt.xlabel("Record Number")
    plt.ylabel("RSSI Value")

    plt.show()