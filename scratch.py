#original implementation, looking ahead by mval

   
    for count, x in enumerate(ach[:len(ach) - mval]): #more than 1M; only false positives and true negatives. #29 is related to nval, mval
        testrange = ach[count:count+mval] 
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            fp.append(x)
        if (length < nval):
            tn.append(x)

    for count, x in enumerate(bch[:len(bch) - mval]): #only true positives and false negatives
        testrange = bch[count:count+mval] 
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            tp.append(x) 
        if (length < nval):
            fn.append(x)



#new implementation, split window

    for count, x in enumerate(ach[floor(mval/2):len(ach) - floor(mval/2)]): #more than 1M; only false positives and true negatives. #29 is related to nval, mval
        testrange = ach[count - floor(mval/2):count+floor(mval/2)] 
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            fp.append(x)
        if (length < nval):
            tn.append(x)

    for count, x in enumerate(bch[floor(mval/2):len(ach) - floor(mval/2)]): #only true positives and false negatives
        testrange = bch[count - floor(mval/2):count+floor(mval/2)]
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            tp.append(x) 
        if (length < nval):
            fn.append(x)


Original :
Max AUC of 0.9145814446110395, at ratio 0.7461538461538462, with n = 37, m = 50
Accuracy of 0.855427974947808
Smallest distance to (1,0) is 0.08256680325751965 at index 22 of 50
Best threshold is -58 dbm
Before 150 data set, min val:-88, max val:-39, mean:-56.1232741617357, median:-57.0, std:5.376513201958254
After 150 data set , min val:-83, max val:-56, mean:-60.07784431137725, median:-59.0, std:3.7049258013253006


new version:
Max AUC of 0.8654938078172445, at ratio 0.7461538461538462, with n = 37, m = 50
Accuracy of 0.8413865546218487
Smallest distance to (1,0) is 0.10063290021891108 at index 22 of 50
Best threshold is -58 dbm
Before 150 data set, min val:-88, max val:-39, mean:-56.1232741617357, median:-57.0, std:5.376513201958254
After 150 data set , min val:-83, max val:-56, mean:-60.07784431137725, median:-59.0, std:3.7049258013253006