# README

Curveinside150 and curveoutside150 contain data data taken by sweeping one of the devices at a radius slightly inside 150cm and slightly outside 150cm. They were swept ~150 degrees around the stationary device, from stationary device right, to left, and back again for about ~250 packets. Each CSV was wholly inside 150cm or outside 150cm.