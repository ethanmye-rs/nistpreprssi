sample no     encounter     description
1-50          close         changing the orientation and the two devices
50-100        close         one device in pocket
100-200       far           stationary environment
200-257       far           changing environment walking (multipath)
257-300       close         human body in the middle of devices
300-350       close         backpack