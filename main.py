import matplotlib
import os
import numpy as np
from math import floor
from glob import glob
from scipy.integrate import simps, trapz
from matplotlib import pyplot as plt

#this analyzer code only works with csvs in the "new" datafolder.

#all false positives --  any tests in the >150 data set that are positive 
#all flase negatives -- any tests in the <150 data set that are negative
#all true positives --   any tests in the <150 data set that are positive 
#all true negatives --   any tests in the >150 data set that are negative

# threshold for danger is 1.5M

#search method for picking "good channels"
    #basic stats, find variance of channel, mean. Sort by best RSSI, pick good channels with variances in lowest x%.
    #Need more channels to test this

def stitchcsvs(df, choice): # should pass in data dir
    '''stitches all csvs in a data directory df into one, dumps it into main data directory with choice label'''
    if choice == 'before' or 'after':
        with open(os.path.join(df, choice + ".csv"), 'a') as merged:
            for csv in glob(os.path.join(df, choice, '*.csv')):
                merged.write("\n")
                for line in open(csv, 'r'):
                    merged.write(line)
    if choice == 'both':
        with open(os.path.join(df, 'before' + ".csv"), 'a') as merged:
            for csv in glob(os.path.join(df, 'before', '*.csv')):
                merged.write("\n")
                for line in open(csv, 'r'):
                    merged.write(line)
        with open(os.path.join(df, 'after' + ".csv"), 'a') as merged:
            for csv in glob(os.path.join(df, 'after', '*.csv')):
                merged.write("\n")
                for line in open(csv, 'r'):
                    merged.write(line)
    else:
        pass

def delacech(df, choice, beforechannel, afterchannel):
    ''' Pull apart a csv into channel data. needs a channeldict dict to store stuff in'''
    with open(os.path.join(df, choice + ".csv"), 'r') as f:
        next(f)
        if choice == 'before':
            for line in f:
                try:
                    ch, rssi = line.strip().split(',')
                    beforechannel[int(ch)].append(rssi)
                except ValueError:
                    next(f) #skip anything that comes out weird
        if choice == 'after':
            for line in f:
                try:
                    ch, rssi = line.strip().split(',')
                    afterchannel[int(ch)].append(rssi)
                except ValueError:
                    next(f) #skip anything that comes out weird

def get_stats(ch):
    return np.amin(ch), np.amax(ch), np.mean(ch), np.median(ch), np.std(ch)
    #maybe should add np.percentile for 25/75.

def get_tpr_fpr(bch, ach, mval, nval, threshold):
    ''' Get True positive, false positive, true negative, and false negative values. '''
    tp = []
    tn = []
    fp = []
    fn = []

    for count, x in enumerate(ach[:len(ach) - mval]): #more than 1M; only false positives and true negatives. #29 is related to nval, mval
        testrange = ach[count:count+mval]
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            fp.append(x)
        if (length < nval):
            tn.append(x)

    for count, x in enumerate(bch[:len(bch) - mval]): #only true positives and false negatives
        testrange = bch[count:count+mval] 
        length = len([i for i in testrange if i >= threshold])
        if (length >= nval):
            tp.append(x) 
        if (length < nval):
            fn.append(x)

    #print(f'true positive:{len(tp)}, false positive:{len(fp)}, true negative:{len(tn)}, false negative:{len(fn)}, sum: {len(tp) + len(fp) + len(tn) + len(fn)}')
    return tp, fp, tn, fn

def get_auc(tpr, fpr):
    '''find area under the curve (auc) for a tpr, fpr list.'''
    return abs(trapz(tpr, fpr)) #needs abs value, integrating backwards.

#driver code
if __name__ == "__main__":

    analysisdir = r'csv\new\garage'
    channelchoice = 3 #largely ignored, now looking at all channels.
    mnscale = 25 # this is arbitrary; pick mnratio based on max number of samples, pick m to be 5% of dataset size.

    #stitchcsvs(analysisdir, 'both') #create new stiched csvs, only needs to be run once. If analysisdir doesn't have a before/after csv in it, run this.

    afterchannel = {n: [] for n in range(0,37)}
    beforechannel = {n: [] for n in range(0,37)}    

    auclist = []
    delacech(analysisdir, 'before', beforechannel, afterchannel)
    delacech(analysisdir, 'after', beforechannel, afterchannel)

    mnratio = list(np.linspace(0.01,1,10))
    m = mnscale
    #n = [floor(mnscale * mnratio[i]) for i in range(0, len(mnratio))]
    n = (3,) #needs to be an iterable
    dblow = -80 #dBm search range to check
    dbhigh = -30

    #combine all channels
    beforech = sum(beforechannel.values(), [])
    afterch =  sum(afterchannel.values(), [])
    #change to ints. Should combine this w/ above
    beforech = [int(i) for i in beforech]
    afterch = [int(i) for i in afterch]

    #find best m,n ratio, not really used if n=(x, )
    for nval in n: 
        tpr = []
        fpr = []
        fnr = []
        for threshold in range(dblow, dbhigh):
            a,b,c,d = get_tpr_fpr(beforech, afterch, m, nval, threshold)
            tpr.append(len(a) / (len(a) + len(d)))
            fpr.append(len(b) / (len(b)+len(c)))

        auclist.append(get_auc(tpr, fpr))

    bestauc = np.amax(auclist)
    print(f"Max AUC of {bestauc}, at ratio {mnratio[auclist.index(bestauc)]}, with n = {n[auclist.index(bestauc)]}, m = {m}")
    
    bestn = n[auclist.index(bestauc)]
    print((list(zip(fpr, tpr, range(dblow, dbhigh)))))
    tpr.clear() #clear out last tpr, fpr values to find new best threshold
    fpr.clear()

    #find closest threshold to (0,1) (optimum classifier) for m,n:
    for threshold in range(dblow, dbhigh):
            a,b,c,d = get_tpr_fpr(beforech, afterch, m, bestn, threshold)
            tpr.append(len(a) / (len(a) + len(d)))
            fpr.append(len(b) / (len(b)+len(c)))
            fnr.append(len(d) / (len((a+d))))
            #print(len((a + d)) / len((a + b + c + d)))
            #print(threshold, len(a), len(b), len(c), len(d))
            
    distances = [(0 - px)**2 + (1 - py)**2 for px, py in zip(fpr, tpr)] # really distance ^2
    lowestdist = np.amin(distances)
    thresh = range(dblow, dbhigh)[distances.index(lowestdist) - 0]
    thresh2 = range(dblow, dbhigh)[distances.index(lowestdist) - 1]

    a,b,c,d = get_tpr_fpr(beforech, afterch, m, bestn, thresh)
    print(f"Accuracy of {len((a + c)) / len((a + b + c + d))}")
    print(f"false negative rate of {(len(d) / len((a+b+c+d))) * 100} % at threshold {thresh}")
    print(f"false positive rate of {(len(b) / len((a+b+c+d))) * 100} % at threshold {thresh}")


    print(f"Smallest distance to (1,0) is {lowestdist} at index {distances.index(lowestdist)} of {len(distances)}")
    print(f'Best threshold is {range(dblow, dbhigh)[distances.index(lowestdist)]} dBm')

    print(f"Before 150 data set, min val:{get_stats(beforech)[0]}, max val:{get_stats(beforech)[1]}, mean:{get_stats(beforech)[2]}, median:{get_stats(beforech)[3]}, std:{get_stats(beforech)[4]}")
    print(f"After 150 data set , min val:{get_stats(afterch)[0]}, max val:{get_stats(afterch)[1]}, mean:{get_stats(afterch)[2]}, median:{get_stats(afterch)[3]}, std:{get_stats(afterch)[4]}")

    plt.scatter(fpr, tpr)
    plt.scatter(fpr[distances.index(lowestdist)], tpr[distances.index(lowestdist)], c = "red")
    #print(f"AUC for ROC : {get_auc(tpr, fpr)}")
    plt.title("ROC Curve")
    plt.annotate(f"-{range(dblow, dbhigh)[distances.index(lowestdist)]}dbm", (fpr[distances.index(lowestdist)], tpr[distances.index(lowestdist)]))
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.annotate(f'M:{m}, N:{bestn}', (.5,0.5))
    plt.show()

    #plt.scatter(fnr, tpr)
    #plt.xlabel("False Negative Rate")
    #plt.ylabel("True Positive Rate")

    #plt.show()
