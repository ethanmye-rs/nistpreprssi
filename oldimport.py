rssilist = []
chlist = []
rssilist2 = []
chlist2 = []

#after data

with open(r'csv\garage\NIST0001\stitched\after.csv', 'r') as f:
    next(f) #throw out first descriptor
    for count, line in enumerate(f):
        time, epoch_time, boot_time, rssi, ch, mac = line.split(',')
        rssilist.append(rssi)
        chlist.append(ch)

#before data
with open(r'csv\garage\NIST0001\stitched\before.csv', 'r') as f:
    next(f) #throw out first descriptor
    for count, line in enumerate(f):
        time, epoch_time, boot_time, rssi, ch, mac = line.split(',')
        rssilist2.append(rssi)
        chlist2.append(ch)

beforech37 = []
beforech38 = []
beforech39 = []

afterch37 = []
afterch38 = []
afterch39 = []


#what a bodge, fix once have more channels
for i, j in zip(chlist2, rssilist2):
    if int(i) > 38:
        beforech39.append(int(j))

    if (int(i) < 39) and (int(i) > 37):
        beforech38.append(int(j))

    if int(i) < 38:
        beforech37.append(int(j))

for i, j in zip(chlist, rssilist):
    if int(i) > 38:
        afterch39.append(int(j))

    if (int(i) < 39) and (int(i) > 37):
        afterch38.append(int(j))

    if int(i) < 38:
        afterch37.append(int(j))