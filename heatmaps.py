import matplotlib.pyplot as plt
import numpy as np
import numpy.random
from glob import glob
from main import stitchcsvs
import os

#https://stackoverflow.com/questions/2369492/generate-a-heatmap-in-matplotlib-using-a-scatter-data-set

if __name__ == "__main__":
    df = r'csv\new\before'
    choice = 'after'


    #stitchcsvs(choice, 'after')
    _ch = []
    _rssi = []

    for count, csv in enumerate(glob(os.path.join(df, choice, '*.csv'))):
        with open(csv) as f:
            next(f)
            for line in f:
                try:
                    ch, rssi = line.strip().split(',')
                    _ch.append(int(ch))
                    _rssi.append(int(rssi))
                except ValueError:
                    next(f) #skip anything that comes out weird

        chdata = np.array(_ch)
        rssidata = np.array(_rssi)
        
        heatmap, xedges, yedges = np.histogram2d(chdata, rssidata, bins= [35, 30], density = True)
        extent = [xedges[0], xedges[-1], -40, -70]



        plt.clf()

        plt.xticks(np.arange(0, 37, dtype=np.int))
        plt.yticks(np.arange(-70, -30, dtype=np.int))
        plt.xlabel("Channel")
        plt.ylabel("RSSI, dBm")
        plt.title(csv[-7:-4] + " cm")
        plt.imshow(heatmap.T, extent=extent, origin='lower', cmap = 'plasma', aspect = 'equal')
        plt.show()
