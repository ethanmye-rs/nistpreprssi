import matplotlib.pyplot as plt
import numpy as np
import numpy.random
from math import floor
from main import stitchcsvs
import os
from glob import glob

#https://stackoverflow.com/questions/2369492/generate-a-heatmap-in-matplotlib-using-a-scatter-data-set

if __name__ == "__main__":
    df = r'csv\new\garage'
    choice = 'before'

    #stitchcsvs(choice, 'after')

    _ch = []
    _rssi = []

    fig, axs = plt.subplots(len(glob(os.path.join(df, choice, '*.csv'))),1, sharex = True)

    for count, csv in enumerate(glob(os.path.join(df, choice, '*.csv'))):
        with open(csv) as f:
            next(f)
            for line in f:
                try:
                    ch, rssi = line.strip().split(',')
                    _ch.append(int(ch))
                    _rssi.append(int(rssi))
                except ValueError:
                    next(f) #skip anything that comes out weird

        chdata = np.array(_ch)
        rssidata = np.array(_rssi)
        _ch = []
        _rssi = []

        #print(np.ptp(rssidata))

        heatmap, xedges, yedges = np.histogram2d(chdata, rssidata, bins= [37, 10], density = True)
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        axs[count].imshow(heatmap.T, extent=extent, origin='lower', cmap = 'plasma', aspect = 'auto')
        axs[count].set_title(csv[-7:-4], loc = 'right')
    plt.show()
